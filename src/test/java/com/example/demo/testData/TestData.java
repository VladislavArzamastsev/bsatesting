package com.example.demo.testData;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TestData {

    public static final Long NOT_EXISTING_ID = -100500L;

    private TestData(){}

    public static List<ToDoEntity> getAllEntities() {
        List<ToDoEntity> testToDos = new ArrayList<>();
        testToDos.add(new ToDoEntity(0L, "Test 1"));
        ToDoEntity toDo = new ToDoEntity(1L, "Test 2");
        toDo.completeNow();
        testToDos.add(toDo);
        return testToDos;
    }

    public static List<ToDoResponse> getAllResponses() {
        return getAllEntities().stream()
                .map(ToDoEntityToResponseMapper::map)
                .collect(Collectors.toList());
    }

    public static List<ToDoSaveRequest> getAllSaveRequests(){
        return getAllEntities().stream()
                .map(e -> {
                    ToDoSaveRequest toDoSaveRequest = new ToDoSaveRequest();
                    toDoSaveRequest.id = e.getId();
                    toDoSaveRequest.text = e.getText();
                    return toDoSaveRequest;
                })
                .collect(Collectors.toList());
    }
}
