package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;

import com.example.demo.service.ToDoService;
import com.example.demo.testData.TestData;

import com.example.demo.exception.ToDoNotFoundException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.example.demo.repository.ToDoRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@AutoConfigureMockMvc
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        when(toDoRepository.findAll()).thenReturn(
                Collections.singletonList(
                        new ToDoEntity(1L, testText)
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Nested
    class GetOneTest {

        @Test
        void whenIdNotPresent_thenExceptionIsHandled() throws Exception {
            Throwable exception = new ToDoNotFoundException(TestData.NOT_EXISTING_ID);

            mockMvc
                    .perform(get(String.format("/todos/%d", TestData.NOT_EXISTING_ID)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                    .andExpect(content().string(exception.getMessage()));
        }

        @Test
        void whenIdIsPresent_thenReturnRight() throws Exception {
            List<ToDoEntity> allEntities = TestData.getAllEntities();
            ToDoEntity toDoEntity = allEntities.get(0);

            when(toDoRepository.findById(toDoEntity.getId())).thenReturn(Optional.of(toDoEntity));

            mockMvc
                    .perform(get(String.format("/todos/%d", toDoEntity.getId())))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("text").value(toDoEntity.getText()))
                    .andExpect(jsonPath("id").isNumber())
                    .andExpect(jsonPath("id").value(toDoEntity.getId()))
                    .andExpect(jsonPath("completedAt").value(toDoEntity.getCompletedAt()));
        }
    }

}
