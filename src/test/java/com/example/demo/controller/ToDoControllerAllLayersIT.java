package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ToDoControllerAllLayersIT {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void getAll() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/todos"), HttpMethod.GET, entity, String.class);

        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy - HH:mm:ss Z");

        String expected = String.format(
                "[{\"id\":1,\"text\":\"Wash the dishes\",\"completedAt\":null}," +
                        "{\"id\":2,\"text\":\"Learn to test Java app\",\"completedAt\":\"%s\"}]", dateTimeFormatter.format(now));
        String actual = replaceNonNullCompletedAtWithCurrentTime(response.getBody(), now, dateTimeFormatter);

        JSONAssert.assertEquals(new JSONArray(expected), new JSONArray(actual), false);
    }

    private String replaceNonNullCompletedAtWithCurrentTime(String json, ZonedDateTime now, DateTimeFormatter writingDateFormat) {
        ObjectMapper objectMapper = new ObjectMapper();
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(new ZonedDateTimeSerializer(writingDateFormat));
        objectMapper.registerModule(javaTimeModule);
        objectMapper.disable(SerializationFeature.WRITE_DATES_WITH_ZONE_ID);
        objectMapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));

        try {
            List<ToDoResponse> responses = objectMapper.readValue(json, new TypeReference<>() {
            });
            responses.forEach(r -> {
                if (r.completedAt != null) {
                    r.completedAt = now;
                }
            });
            return objectMapper.writeValueAsString(responses);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getOne() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/todos/1"), HttpMethod.GET, entity, String.class);

        String expected = "{\"id\":1,\"text\":\"Wash the dishes\",\"completedAt\":null}";

        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }


}
