package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;
import com.example.demo.exception.ToDoNotFoundException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import com.example.demo.service.ToDoService;
import com.example.demo.testData.TestData;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoService toDoService;

	@Test
	void whenGetAll_thenReturnValidResponse() throws Exception {
		String testText = "My to do text";
		Long testId = 1L;
		when(toDoService.getAll()).thenReturn(
				Collections.singletonList(
						ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
				)
		);
		
		this.mockMvc
			.perform(get("/todos"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].text").value(testText))
			.andExpect(jsonPath("$[0].id").isNumber())
			.andExpect(jsonPath("$[0].id").value(testId))
			.andExpect(jsonPath("$[0].completedAt").doesNotExist());
	}

	@Nested
	class GetOneTest{

		@Test
		void whenIdNotPresent_thenExceptionIsHandled() throws Exception {
			Throwable exception = new ToDoNotFoundException(TestData.NOT_EXISTING_ID);
			doThrow(exception).when(toDoService).getOne(TestData.NOT_EXISTING_ID);

			ToDoControllerIT.this.mockMvc
					.perform(delete(String.format("/todos/%d", TestData.NOT_EXISTING_ID)))
					.andExpect(status().isOk());
		}

		@Test
		void whenIdIsPresent_thenReturnRight() throws Exception {
			List<ToDoResponse> allResponses = TestData.getAllResponses();
			ToDoResponse toDoResponse = allResponses.get(0);

			when(toDoService.getOne(toDoResponse.id)).thenReturn(toDoResponse);

			mockMvc
					.perform(get(String.format("/todos/%d", toDoResponse.id)))
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("text").value(toDoResponse.text))
					.andExpect(jsonPath("id").isNumber())
					.andExpect(jsonPath("id").value(toDoResponse.id))
					.andExpect(jsonPath("completedAt").value(toDoResponse.completedAt));
		}
	}

	@Nested
	class CompleteTest{

		@Test
		void whenIdNotPresent_thenExceptionIsHandled() throws Exception{
			Throwable exception = new ToDoNotFoundException(TestData.NOT_EXISTING_ID);
			doThrow(exception).when(toDoService).getOne(TestData.NOT_EXISTING_ID);

			mockMvc
					.perform(put(String.format("/todos/%d/complete", TestData.NOT_EXISTING_ID)))
					.andExpect(status().isOk());
		}

		@Test
		void whenIdIsPresent_thenReturnRight() throws Exception {
			List<ToDoSaveRequest> allSaveRequests = TestData.getAllSaveRequests();
			List<ToDoResponse> allResponses = TestData.getAllResponses();
			ToDoSaveRequest toDoSaveRequest = allSaveRequests.get(0);
			ToDoResponse toDoResponse = allResponses.get(0);
			toDoResponse.completedAt = ZonedDateTime.now();

			when(toDoService.completeToDo(toDoSaveRequest.id)).thenReturn(toDoResponse);

			mockMvc
					.perform(put(String.format("/todos/%d/complete", toDoResponse.id)))
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("text").value(toDoResponse.text))
					.andExpect(jsonPath("id").isNumber())
					.andExpect(jsonPath("id").value(toDoResponse.id))
					.andExpect(jsonPath("completedAt").exists());
		}
	}

}
