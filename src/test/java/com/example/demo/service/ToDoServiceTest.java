package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.*;
import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;

import com.example.demo.testData.TestData;

import org.junit.jupiter.api.Nested;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.repository.ToDoRepository;

class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    private List<ToDoEntity> allEntities;

    //executes before each test defined below
    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        this.toDoService = new ToDoService(toDoRepository);
        this.allEntities = TestData.getAllEntities();
    }

    @Test
    void whenGetAll_thenReturnAll() {
        //mock

        when(toDoRepository.findAll()).thenReturn(allEntities);

        //call
        var todos = toDoService.getAll();

        //validate
        assertEquals(todos.size(), allEntities.size());
        for (int i = 0; i < todos.size(); i++) {
            assertThat(todos.get(i), samePropertyValuesAs(
                    ToDoEntityToResponseMapper.map(allEntities.get(i))
            ));
        }
    }

    @Nested
    class UpsertTest {

        @Test
        void whenWithId_thenReturnUpdated() throws ToDoNotFoundException {
            //mock
            ToDoEntity expectedToDo = new ToDoEntity(0L, "New Item");
            when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
                Long id = i.getArgument(0, Long.class);
                if (id.equals(expectedToDo.getId())) {
                    return Optional.of(expectedToDo);
                } else {
                    return Optional.empty();
                }
            });
            when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
                ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
                Long id = arg.getId();
                if (id != null) {
                    if (!id.equals(expectedToDo.getId()))
                        return new ToDoEntity(id, arg.getText());
                    expectedToDo.setText(arg.getText());
                    return expectedToDo; //return valid result only if we get valid id
                } else {
                    return new ToDoEntity(40158L, arg.getText());
                }
            });

            //call
            var toDoSaveRequest = new ToDoSaveRequest();
            toDoSaveRequest.id = expectedToDo.getId();
            toDoSaveRequest.text = "Updated Item";
            var todo = toDoService.upsert(toDoSaveRequest);

            //validate
            assertSame(todo.id, toDoSaveRequest.id);
            assertEquals(todo.text, toDoSaveRequest.text);
        }

        @Test
        void whenNoId_thenReturnNew() throws ToDoNotFoundException {
            //mock
            var newId = 0L;
            when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
                Long id = i.getArgument(0, Long.class);
                if (id == newId) {
                    return Optional.empty();
                } else {
                    return Optional.of(new ToDoEntity(newId, "Wrong ToDo"));
                }
            });
            when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
                ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
                Long id = arg.getId();
                if (id == null)
                    return new ToDoEntity(newId, arg.getText());
                else
                    return new ToDoEntity();
            });

            //call
            var toDoDto = new ToDoSaveRequest();
            toDoDto.text = "Created Item";
            var result = toDoService.upsert(toDoDto);

            //validate
            assertEquals((long) result.id, newId);
            assertEquals(result.text, toDoDto.text);
        }

        @Test
        void whenNotExistingId_thenThrow(){
            ToDoSaveRequest toDoSaveRequest = new ToDoSaveRequest();
            toDoSaveRequest.id = TestData.NOT_EXISTING_ID;
            toDoSaveRequest.text = "";
            assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(toDoSaveRequest));
        }

    }

    @Nested
    class CompleteTest {

        @Test
        void whenComplete_thenReturnWithCompletedAt() throws ToDoNotFoundException {
            ZonedDateTime startTime = ZonedDateTime.now(ZoneOffset.UTC);
            //mock
            ToDoEntity todo = new ToDoEntity(0L, "Test 1");
            when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));
            when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
                ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
                Long id = arg.getId();
                if (id.equals(todo.getId())) {
                    return todo;
                } else {
                    return new ToDoEntity();
                }
            });

            //call
            var result = toDoService.completeToDo(todo.getId());

            //validate
            assertSame(result.id, todo.getId());
            assertEquals(result.text, todo.getText());
            assertTrue(result.completedAt.isAfter(startTime));
        }

        @Test
        void whenIdNotPresent_thenTrows(){
            assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(TestData.NOT_EXISTING_ID));
        }
    }

    @Nested
    class DeleteOneTest {

        @Test
        void repositoryDeleteCalled() {
            //call
            var id = 0L;
            toDoService.deleteOne(id);

            //validate
            verify(toDoRepository, times(1)).deleteById(id);
        }

        @Test
        void whenIdNotPresent_thenDoesNotThrows() {
            assertDoesNotThrow(() -> toDoService.deleteOne(TestData.NOT_EXISTING_ID));
        }

        @Test
        void whenIdPresent_thenDelete() {
            int indexOfExisting = 0;
            List<ToDoEntity> expected = new ArrayList<>(allEntities);
            expected.remove(indexOfExisting);

            ToDoEntity existing = allEntities.get(indexOfExisting);
            toDoService.deleteOne(existing.getId());

            when(toDoRepository.findAll()).thenReturn(expected);
            List<ToDoResponse> all = toDoService.getAll();

            assertEquals(expected.size(), all.size());
            for(int i = 0; i < expected.size(); i++){
                assertThat(all.get(i), samePropertyValuesAs(
                        ToDoEntityToResponseMapper.map(expected.get(i))
                ));
            }
        }

    }

    @Nested
    class GetOneTest {

        @Test
        void whenGetOne_thenReturnCorrectOne() throws ToDoNotFoundException {
            //mock
            var todo = new ToDoEntity(0L, "Test 1");
            when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));

            //call
            var result = toDoService.getOne(0L);

            //validate
            assertThat(result, samePropertyValuesAs(
                    ToDoEntityToResponseMapper.map(todo)
            ));
        }

        @Test
        void whenIdNotFound_thenThrowNotFoundException() {
            assertThrows(ToDoNotFoundException.class, () -> toDoService.getOne(1L));
        }

    }

}
